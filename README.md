# BWLS WebApp


Setting up BWLS WebApp project (examples here are for MAC installation)
Bitbucket location: [BWLS WebApp](https://bitbucket.org/blackwelllearning/bwls-webapp)
In this repo are stored the latest versions of Tomcat and JAVA that can be used for production.

## Table of Contents


1. [Prerequisites](#markdown-header-prerequisites)
2. [GIT](#markdown-header-git)
3. [Java](#markdown-header-java)
4. [Apache Tomcat](#markdown-header-apache-tomcat)
5. [Gradle](#markdown-header-gradle)
6. [Node](#markdown-header-node)
7. [gulp](#markdown-header-gulp)
8. [nvm](#markdown-header-nvm)
9. [Download project](#markdown-header-download-project)
10. [Configure your Local](#markdown-header-configure-your-Local)

## Prerequisites
Be wary of your downloads and when available, you should verify the integrity of the downoaded file using the ahs provided. Here is how to:
On techradar website: (http://www.techradar.com/how-to/how-to-check-a-files-checksum-on-mac)
Or osxdaily website: (http://osxdaily.com/2009/10/13/check-md5-hash-on-your-mac/)


## [GIT](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
The easiest is probably to install the Xcode Command Line Tools. On Mavericks (10.9) or above you can do this simply by trying to run git from the Terminal the very first time.


`$ git --version`


If you don't have it installed already, it will prompt you to install it.


If you want a more up to date version, you can also install it via a binary installer. A macOS Git installer is maintained and available for download at the [Git website](http://git-scm.com/download/mac).


You can also install it as part of the GitHub for Mac install. Their GUI Git tool has an option to install command line tools as well. You can download that tool from the [GitHub](http://mac.github.com) for Mac.
Another wellknown GUI is [Sourcetree](https://www.sourcetreeapp.com).


## Java

The base code has been developed using JAVA 7.0.X.
You can find it on the archive's download page of the [oracle website](http://www.oracle.com/technetwork/java/javase/downloads/java-archive-downloads-javase7-521261.html)


Dowload the installer and let everything by default.
It should install the JAVA Control Panel in your setting app and java at this location:


`/Library/Java/JavaVirtualMachines/jdk1.7.X.jdk/Contents/Home`


## Apache Tomcat
To run the above code you will need to install Tomcat 7.X.X or above from the Apache
Note that different version of Tomcat might not support (be supported) by every version of JAVA:
Look at the [apache doc](http://tomcat.apache.org/whichversion.html).




[Tomcat website](http://tomcat.apache.org) and the [documentation](https://tomcat.apache.org/tomcat-7.0-doc/index.html) to install it.



Eventually easier with this [tutorials](https://wolfpaulus.com/mac/tomcat) by Wolf Paulus


## Gradle
[Fom gradle website](https://gradle.org/install):


Gradle runs on all major operating systems and requires only a Java JDK or JRE version 7 or higher to be installed.
To check, run:
```
$ java -version
java version "1.7.0.X"
```


For a mac user, the easiest way to install it would be via [Homebrew](https://brew.sh/) (package manager):
```sh
$ brew install gradle
```
But other methods exists.


Once installed. **From project ROOT directory** run:
```sh
$ ./gradlew
```
This will download gradle dependencies and start gradle.

Even if you have a later version of JAVA installed,
to build the project with the required JAVA sdk, you will need to create an alias (or type in the whole command every time)

```sh
$ gradle clean build -Penv=loc1 -x test -Dorg.gradle.java.home=PATH/TO/REQUIERED/JAVA/jdk1.7.0_80.jdk/Contents/Home
```

Mine looks like:
```sh
alias bwBuild="gradle clean build -Penv=loc1 -x test -Dorg.gradle.java.home=/Library/Java/JavaVirtualMachines/jdk1.7.0_80.jdk/Contents/Home"
```


Then on everyday-basis, you should run:

```sh
$ ./gradlew clean build -x test
```
Where clean and build are defined tasks and -x test a parameter passed down to gradle specifying not to run the test task.

Or, if you set up an alias:
```sh
$ bwBuild
```


Note:
You have to be on a registered internal address (VPN)
I am using gradle not gradle wrapper (gradlew), I don't know the differences.


## Node
The front end environment is built with javascript tools
These tools run with node to:
    - Compiling sass files into css,
    - Compiling “next generation” javascript into IE9 ~like understandable javascript
    - Concatanating, testing, minifying


Note:
For a developer's machine, we recomand installing `nvm` first and `node` from nvm's utilities.
You might have differnet projects that would have different node requirements.
See nvm below.

If you know you won't need to change node (and all/any of it's dependencies) here are the requirements:


Node.js version 5.0.0
[Download link](https://nodejs.org/download/release/v5.0.0) (pick the one for your OS, mac uses darwin-x64 version)

This version of node.js comes packaged with `NPM 3.3.6`


## Gulp

If you have installed `nvm`, from the `frontend/` directory you **MUST** run nvm first
```sh
$ nvm use
```
Otherwise your system won't know that you have node installed,
nvm won't map it to the correct version.

Note:
sometimes, Gulp installed via node (from nvm) doesn't work!!
In this case you need Gulp to be installed globally on your system:
```sh
$ npm install -g gulp
```

The same goes for the cli plugin:
```sh
$ npm install -g gulp-cli
```


## nvm

**nvm** stands for **N**ode **V**ersion **M**anager.
As developers we come across different projects with different setups and requirements, but only 1 machine (usually).
nvm allow you to install several versions of nodes.js on your machine.
It basically install any version of node in its on folder and add some alliases for you to use the "right" version.

[The github page for nvm](https://github.com/creationix/nvm)


### Their readme page states:
```sh
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
```

The script clones the nvm repository to `~/.nvm` and adds the source line to your profile.
(~/.bash_profile, ~/.zshrc, ~/.profile, or ~/.bashrc).

```sh
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm
```


### Once nvm installed,
To download, compile, and install the specific release of node, run:
```sh
$ nvm install v5.0.0
```

It will download the requiered node, npm version for our project.
How does it relate to the project:

If you run multiple version of node, be sure to run `$ nvm use` from the `path/to/the/project/bwls-webapp/frontend/` folder before running any node related commands (install, run, etc).
`$ nvm use` will look for the `.nvmrc` file in which the vesion of node is stored.
Not doing so might triggers error in node plugins.

## Download Project
bitbucket: https://bitbucket.org/blackwelllearning/bwls-webapp


## Configure your Local

### Apache httpd.conf
Locate your `httpd.conf file`.
Usual location on Mac: `/etc/apache2/httpd.conf`.

- Enable this modules (L: ~132 and ~166):


```sh
LoadModule proxy_module libexec/apache2/mod_proxy.so
LoadModule proxy_connect_module libexec/apache2/mod_proxy_connect.so
LoadModule proxy_ftp_module libexec/apache2/mod_proxy_ftp.so
LoadModule proxy_http_module libexec/apache2/mod_proxy_http.so
LoadModule proxy_fcgi_module libexec/apache2/mod_proxy_fcgi.so
LoadModule proxy_scgi_module libexec/apache2/mod_proxy_scgi.so
LoadModule proxy_wstunnel_module libexec/apache2/mod_proxy_wstunnel.so
LoadModule proxy_ajp_module libexec/apache2/mod_proxy_ajp.so
LoadModule proxy_balancer_module libexec/apache2/mod_proxy_balancer.so
LoadModule proxy_express_module libexec/apache2/mod_proxy_express.so



LoadModule userdir_module libexec/apache2/mod_userdir.so
LoadModule rewrite_module libexec/apache2/mod_rewrite.so
```

- Update default directive:

```sh
<Directory />
AllowOverride all  #(from none to all)
Require all granted  #(from denied to granted)
</Directory>
```

- ajp configuration
Needs to map some jsp, jackets resources and specific Routes.
Add the following lines at the bottom of the apache httpd.conf file

```sh
ProxyPass           /jacket/    http://catmantest2.blackwell.co.uk/cm/jacket/
ProxyPassReverse    /jacket/    http://catmantest2.blackwell.co.uk/cm/jacket/

ProxyPass           /bookshop    ajp://localhost:8009/bookshop
ProxyPassReverse    /bookshop    ajp://localhost:8009/bookshop

ProxyPass           /jsp    ajp://localhost:8009/jsp
ProxyPassReverse    /jsp    ajp://localhost:8009/jsp
```


### Tomcat config files:
Get the `/opt` folder from any dev server instance
**TODO**, *link to the dev servers logins details. TWIKI page?*

Create symlink from `/opt/bosweb/config/homePageLinks` to the project props `bwls-webapp/env-props/homepageLinks`.
```sh
$ ln -s bwls-webapp/env-props/homepageLinks /opt/bosweb/config/homePageLinks
```

Manually create the resources folder and path (gulp doesn't have write permission on `/` folder on the Mac:
```sh
$ sudo mkdir /nasdev/bosweb/dev1/htdocs/resources
```


## Starting to develop:
- Open VPN connection
- clear your previous builts
- run the build command
- start tomcat (default will deploy the .war file on its own)
- start apache (if not already)
- run the gulp --front command


**TODO**, *Need a PC user to implement the relevant parts*